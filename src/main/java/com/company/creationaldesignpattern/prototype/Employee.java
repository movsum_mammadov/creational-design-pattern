package com.company.creationaldesignpattern.prototype;

public interface Employee extends Cloneable {

    public abstract Employee clone();

    public abstract void displayInfo();

}
