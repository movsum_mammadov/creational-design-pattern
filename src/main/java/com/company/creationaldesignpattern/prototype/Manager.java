package com.company.creationaldesignpattern.prototype;

public class Manager implements Employee {

    private final String name;
    private final String department;

    public Manager(String name, String department) {
        this.name = name;
        this.department = department;
    }

    @Override
    public Employee clone() {
        return new Manager(name, department);
    }

    @Override
    public void displayInfo() {
        System.out.println("Manager : " + name + ", Department : " + department);
    }
}
