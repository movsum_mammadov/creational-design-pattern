package com.company.creationaldesignpattern.prototype;

public class Developer implements Employee {

    private String name;
    private String role;

    public Developer(String name, String role) {
        this.name = name;
        this.role = role;
    }

    @Override
    public Employee clone() {
        return new Developer(name, role);
    }

    @Override
    public void displayInfo() {
        System.out.println("Developer : " + name + ", Role : " + role);
    }
}
