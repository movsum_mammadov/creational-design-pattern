package com.company.creationaldesignpattern.abstractfactory.car;

public interface Car {
    void drive();

}
