package com.company.creationaldesignpattern.abstractfactory.car;

public class Sedan implements Car {
    @Override
    public void drive() {
        System.out.println("Driving Sedan");
    }
}
