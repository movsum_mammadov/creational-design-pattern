package com.company.creationaldesignpattern.abstractfactory.car;

public class Limousine implements Car {
    @Override
    public void drive() {
        System.out.println("Driving Limousine");
    }
}
