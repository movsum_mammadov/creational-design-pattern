package com.company.creationaldesignpattern.abstractfactory.factory;

import com.company.creationaldesignpattern.abstractfactory.car.Car;
import com.company.creationaldesignpattern.abstractfactory.engine.Engine;
import com.company.creationaldesignpattern.abstractfactory.car.Limousine;
import com.company.creationaldesignpattern.abstractfactory.engine.LimousineEngine;

public class LimousineFactory extends AbstractFactory {
    @Override
    public Car createCar() {
        return new Limousine();
    }

    @Override
    public Engine createEngine() {
        return new LimousineEngine();
    }
}
