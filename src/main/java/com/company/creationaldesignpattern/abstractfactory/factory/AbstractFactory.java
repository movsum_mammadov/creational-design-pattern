package com.company.creationaldesignpattern.abstractfactory.factory;

import com.company.creationaldesignpattern.abstractfactory.car.Car;
import com.company.creationaldesignpattern.abstractfactory.engine.Engine;

public abstract class AbstractFactory {

    public abstract Car createCar();

    public abstract Engine createEngine();

}
