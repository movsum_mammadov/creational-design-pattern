package com.company.creationaldesignpattern.abstractfactory.factory;

import com.company.creationaldesignpattern.abstractfactory.car.Car;
import com.company.creationaldesignpattern.abstractfactory.engine.Engine;
import com.company.creationaldesignpattern.abstractfactory.car.Sedan;
import com.company.creationaldesignpattern.abstractfactory.engine.SedanEngine;

public class SedanFactory extends AbstractFactory {
    @Override
    public Car createCar() {
        return new Sedan();
    }

    @Override
    public Engine createEngine() {
        return new SedanEngine();
    }
}
