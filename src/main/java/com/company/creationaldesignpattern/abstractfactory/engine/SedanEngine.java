package com.company.creationaldesignpattern.abstractfactory.engine;

public class SedanEngine implements Engine {
    @Override
    public void start() {
        System.out.println("Starting Sedan Engine");
    }
}
