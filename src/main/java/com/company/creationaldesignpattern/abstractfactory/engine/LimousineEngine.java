package com.company.creationaldesignpattern.abstractfactory.engine;

public class LimousineEngine implements Engine {
    @Override
    public void start() {
        System.out.println("Starting Limousine Engine");
    }
}
