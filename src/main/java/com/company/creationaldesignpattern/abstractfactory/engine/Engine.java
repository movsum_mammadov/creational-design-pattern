package com.company.creationaldesignpattern.abstractfactory.engine;

public interface Engine {
    void start();
}
