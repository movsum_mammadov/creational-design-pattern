package com.company.creationaldesignpattern;


import com.company.creationaldesignpattern.abstractfactory.engine.Engine;
import com.company.creationaldesignpattern.abstractfactory.factory.AbstractFactory;
import com.company.creationaldesignpattern.abstractfactory.factory.LimousineFactory;
import com.company.creationaldesignpattern.abstractfactory.factory.SedanFactory;
import com.company.creationaldesignpattern.abstractfactory.car.Car;

public class Main {

    public static void main(String[] args) {

        AbstractFactory limousineFactory= new LimousineFactory();
        AbstractFactory sedanFactory = new SedanFactory();

        Car sedan = sedanFactory.createCar();
        Car limousin=limousineFactory.createCar();

        limousin.drive();
        sedan.drive();

        Engine sedanEngine = sedanFactory.createEngine();
        Engine limousineEngine = limousineFactory.createEngine();

        sedanEngine.start();
        limousineEngine.start();

    }
}
