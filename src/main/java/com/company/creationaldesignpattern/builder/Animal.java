package com.company.creationaldesignpattern.builder;

public class Animal {

    private String name;
    private String species;
    private int age;
    private boolean isWild;

    private Animal() {

    }

    public Animal(String name, String species, int age, boolean isWild) {
        this.name = name;
        this.species = species;
        this.age = age;
        this.isWild = isWild;
    }

    public String getName() {
        return name;
    }

    public String getSpecies() {
        return species;
    }

    public int getAge() {
        return age;
    }

    public boolean isWild() {
        return isWild;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "name='" + name + '\'' +
                ", species='" + species + '\'' +
                ", age=" + age +
                ", isWild=" + isWild +
                '}';
    }

    public static AnimalBuilder builder() {
        return new AnimalBuilder();
    }

    public static class AnimalBuilder {
        private Animal animal = null;

        public AnimalBuilder() {
            this.animal = new Animal();
        }

        public Animal build() {
            return new Animal(animal.name, animal.species, animal.age, animal.isWild);
        }

        public AnimalBuilder name(String name) {
            this.animal.name = name;
            return this;
        }

        public AnimalBuilder species(String species) {
            this.animal.species = species;
            return this;
        }

        public AnimalBuilder age(int age) {
            this.animal.age = age;
            return this;
        }

        public AnimalBuilder animalBuilder(boolean isWild) {
            this.animal.isWild = isWild;
            return this;
        }
    }
}
