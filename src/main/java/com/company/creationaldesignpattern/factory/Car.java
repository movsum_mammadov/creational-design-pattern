package com.company.creationaldesignpattern.factory;

public interface Car {
    void drive();

}
