package com.company.creationaldesignpattern.factory;

public class Limousine implements Car {
    @Override
    public void drive() {
        System.out.println("Driving Limousine");
    }
}
