package com.company.creationaldesignpattern.factory;

public class CarFactory {
    public static Car createCar(String carType) {
        if (carType == null) {
            return null;
        } else if (carType.equalsIgnoreCase("Sedan")) {
            return new Sedan();
        } else if (carType.equalsIgnoreCase("Limousine")) {
            return new Limousine();
        } else {
            throw new RuntimeException(carType + " markasi istehsal olunmur.");
        }
    }

}
